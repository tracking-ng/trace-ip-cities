#!/bin/sh
traceroute $1 | awk '{print $3}' | grep '(' | sed 's:(::g' | sed 's:)::g' > /tmp/traceroute.tmp
for i in $(cat /tmp/traceroute.tmp);do
	echo $(curl -s ipinfo.io/$i/city; curl -s ipinfo.io/$i/region; curl -s ipinfo.io/$i/country)
done | grep -v 400
rm /tmp/traceroute.tmp
