# trace-ip-cities

A simple shell script that list the city, region and country of the servers your request runs through.

To download this script run the following command (should work on every unix system, please tell me if not):
`wget https://gitlab.com/germfree/trace-ip-cities/-/raw/master/trace-ip-cities.sh; chmod +x trace-ip-cities.sh`
and to run it(replace 'openbsd.org' with the domain or ip address you want)
`./trace-ip-cities.sh openbsd.org`
